resource "aws_elb" "env_lb" {
  name               = "${join("-","${list(var.environment_name, "LB")}")}"
  subnets            = ["${aws_subnet.env_subnet_a_public.id}", "${aws_subnet.env_subnet_b_public.id}"]
  security_groups = ["${module.security.ec2_instance_sg_id}"]

  listener {
    instance_port     = 8080
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8080/"
    interval            = 30
  }

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "LB")}")}"
  }
}