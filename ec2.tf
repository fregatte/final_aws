module "security" {
  source = "modules/security"

  environment_name = "${var.environment_name}"
  vpc_id = "${aws_vpc.env_vpc.id}"
}

data "template_file" "ec2_init_script" {
  template = "${file("scripts/ec2_init.sh")}"

  vars {
    package = "nginx"
  }
}