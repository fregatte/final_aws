variable "vpc_id" {}

variable "environment_name" {}

resource "aws_security_group" "ec2_instane_sg" {
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${join("-","${list(var.environment_name, "ec2-sg")}")}"
    EnvironmentName = "${var.environment_name}"
  }

}