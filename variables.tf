variable "ami" {
  default = "ami-0e86606d"
}

variable "region" {
  type = "string"
  default = "us-west-1"
}

variable "availability_zones" {
  type = "list"
  default = ["0", "1"]
}

variable "environment_name" {
  type = "string"
  default = "final-aws"
}

variable "ssh_key_name" {
  type = "string"
  default = "final-aws"
}

variable "ssh_key_path" {
  type = "string"
  default = "~/.ssh/final-aws.pem"
}
