resource "aws_launch_configuration" "env_launch_conf" {
  name            = "${join("-","${list(var.environment_name, "launch-conf")}")}"
  image_id        = "${var.ami}"
  instance_type   = "t2.micro"
  security_groups = ["${module.security.ec2_instance_sg_id}"]
  key_name        = "${var.ssh_key_name}"
  user_data       = "${data.template_file.ec2_init_script.rendered}"
}

resource "aws_autoscaling_group" "env_scaling_group" {
  name                 = "${join("-","${list(var.environment_name, "sg")}")}"
  launch_configuration = "${aws_launch_configuration.env_launch_conf.name}"
  min_size             = 2
  max_size             = 5

  health_check_grace_period = 60
  health_check_type         = "EC2"
  desired_capacity          = 2
  force_delete              = true
  vpc_zone_identifier       = ["${aws_subnet.env_subnet_a.id}", "${aws_subnet.env_subnet_b.id}"]
  load_balancers = ["${aws_elb.env_lb.id}"]

  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_autoscaling_policy" "env_sg-policy-up" {
  name = "${join("-","${list(var.environment_name, "policy-up")}")}"
  autoscaling_group_name = "${aws_autoscaling_group.env_scaling_group.name}"
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "1"
  cooldown = "300"
  policy_type = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "env_policy-alarm-up" {
  alarm_name = "${join("-","${list(var.environment_name, "alarm-up")}")}"
  #alarm_description = "example-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "30"
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.env_scaling_group.name}"
  }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.env_sg-policy-up.arn}"]
}

resource "aws_autoscaling_policy" "env_sg-policy-down" {
  name = "${join("-","${list(var.environment_name, "policy-down")}")}"
  autoscaling_group_name = "${aws_autoscaling_group.env_scaling_group.name}"
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "-1"
  cooldown = "300"
  policy_type = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "env_policy-alarm-down" {
  alarm_name = "${join("-","${list(var.environment_name, "alarm-down")}")}"
  #alarm_description = "example-cpu-alarm-scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "5"
  dimensions = {
    "AutoScalingGroupName" = "${aws_autoscaling_group.env_scaling_group.name}"
  }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.env_sg-policy-down.arn}"]
}
