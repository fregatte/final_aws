resource "aws_vpc" "env_vpc" {
  cidr_block         = "10.5.0.0/16"
  enable_dns_support = true

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "VPC")}")}"
  }
}

data "aws_availability_zones" "env_available_azs" {}


resource "aws_subnet" "env_subnet_a" {
  vpc_id                  = "${aws_vpc.env_vpc.id}"
  cidr_block              = "10.5.1.0/24"
  availability_zone       = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[0]]}"
  map_public_ip_on_launch = false

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "SubnetA")}")}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "env_subnet_b" {
  vpc_id                  = "${aws_vpc.env_vpc.id}"
  cidr_block              = "10.5.2.0/24"
  availability_zone       = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[1]]}"
  map_public_ip_on_launch = false

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "SubnetB")}")}"
  }

  lifecycle {
    create_before_destroy = false
  }
}

resource "aws_subnet" "env_subnet_a_public" {
  vpc_id                  = "${aws_vpc.env_vpc.id}"
  cidr_block              = "10.5.3.0/24"
  availability_zone       = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[0]]}"
  map_public_ip_on_launch = true

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "SubnetA-public")}")}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_subnet" "env_subnet_b_public" {
  vpc_id                  = "${aws_vpc.env_vpc.id}"
  cidr_block              = "10.5.4.0/24"
  availability_zone       = "${data.aws_availability_zones.env_available_azs.names[var.availability_zones[1]]}"
  map_public_ip_on_launch = true

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "SubnetB-public")}")}"
  }

  lifecycle {
    create_before_destroy = false
  }
}


resource "aws_internet_gateway" "env_igw" {
  vpc_id = "${aws_vpc.env_vpc.id}"

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "IGW")}")}"
  }
}

resource "aws_vpc_dhcp_options" "env_vpc_dhcp_options" {
  domain_name         = "${var.environment_name}"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "DHCPOptions")}")}"
  }
}

resource "aws_vpc_dhcp_options_association" "env_vpc_dhcp_association" {
  vpc_id          = "${aws_vpc.env_vpc.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.env_vpc_dhcp_options.id}"
}

resource "aws_default_network_acl" "env_network_acl" {
  default_network_acl_id = "${aws_vpc.env_vpc.default_network_acl_id}"

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "ACL")}")}"
  }

  subnet_ids = [
    "${aws_subnet.env_subnet_a.id}",
    "${aws_subnet.env_subnet_b.id}",
    "${aws_subnet.env_subnet_a_public.id}",
    "${aws_subnet.env_subnet_b_public.id}"
  ]
}

resource "aws_route_table" "env_route_table" {
  vpc_id = "${aws_vpc.env_vpc.id}"

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "RouteTablePublic")}")}"
  }
}

resource "aws_route" "env_route_pub_default" {
  route_table_id         = "${aws_route_table.env_route_table.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.env_igw.id}"
}

resource "aws_route_table_association" "env_rt_table_association_a_public" {
  subnet_id      = "${aws_subnet.env_subnet_a_public.id}"
  route_table_id = "${aws_route_table.env_route_table.id}"
}

resource "aws_route_table_association" "env_rt_table_association_b_public" {
  subnet_id      = "${aws_subnet.env_subnet_b_public.id}"
  route_table_id = "${aws_route_table.env_route_table.id}"
}

resource "aws_route_table" "env_route_table_nat_A" {
  vpc_id = "${aws_vpc.env_vpc.id}"

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "RouteTablePrivateA")}")}"
  }
}

resource "aws_route_table" "env_route_table_nat_B" {
  vpc_id = "${aws_vpc.env_vpc.id}"

  tags {
    EnvironmentName = "${var.environment_name}"
    Name            = "${join("-","${list(var.environment_name, "RouteTablePrivateB")}")}"
  }
}

resource "aws_eip" "nat_eip_A" {
  vpc      = true
  depends_on = ["aws_internet_gateway.env_igw"]
}

resource "aws_eip" "nat_eip_B" {
  vpc      = true
  depends_on = ["aws_internet_gateway.env_igw"]
}

resource "aws_nat_gateway" "env_nat_gw_A" {
  allocation_id = "${aws_eip.nat_eip_A.id}"
  subnet_id     = "${aws_subnet.env_subnet_a_public.id}"
}

resource "aws_nat_gateway" "env_nat_gw_B" {
  allocation_id = "${aws_eip.nat_eip_B.id}"
  subnet_id     = "${aws_subnet.env_subnet_b_public.id}"
}

resource "aws_route" "env_route_private_A" {
  route_table_id         = "${aws_route_table.env_route_table_nat_A.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_nat_gateway.env_nat_gw_A.id}"
}

resource "aws_route" "env_route_private_B" {
  route_table_id         = "${aws_route_table.env_route_table_nat_B.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_nat_gateway.env_nat_gw_B.id}"
}


resource "aws_route_table_association" "env_rt_table_association_a" {
  subnet_id      = "${aws_subnet.env_subnet_a.id}"
  route_table_id = "${aws_route_table.env_route_table_nat_A.id}"
}

resource "aws_route_table_association" "env_rt_table_association_b" {
  subnet_id      = "${aws_subnet.env_subnet_b.id}"
  route_table_id = "${aws_route_table.env_route_table_nat_B.id}"
}