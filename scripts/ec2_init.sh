#!/bin/bash

# user_data file, running under root!

yum update -y
yum install -y git
git clone https://gitlab.com/fregatte/final_aws_ec2_init.git
cd /final_aws_ec2_init
chmod +x ./ec2_init.sh
/bin/bash -c ./ec2_init.sh
